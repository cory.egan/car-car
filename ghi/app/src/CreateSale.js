import React, {useState, useEffect} from 'react';
import './index.css';

function CreateSale() {
    const [automobile, setAutomobile] = useState('')
    const [customer, setCustomer] = useState('')
    const [sales_person, setSalesPerson] = useState('')
    const [sale_price, setSalePrice] = useState('')
    const [salespeople, setSalespeople] = useState([])
    const [sales, setSales] =useState([])
    const [customers, setCustomers] =useState([])
    const [automobiles, setAutomobiles] = useState([])



    const handleAutomobileChange = (e) => {
        const value = e.target.value;
        setAutomobile(value);
    }
    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    }
    const handleSalesPersonChange = (e) => {
        const value = e.target.value;
        setSalesPerson(value)
    }

    const handleSalePriceChange = (e) => {
        const value = e.target.value;
        setSalePrice(value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();

    const data = {
        automobile: {
            automobile_vin: automobile,
            sold: false
        },
        sales_person: sales_person,
        customer: customer,
        sale_price: sale_price
    }



const url = 'http://localhost:8090/api/sales/';
const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
    'Content-Type': 'application/json',
    }
}





const response = await fetch(url, fetchConfig)
console.log(response)
if (response.ok) {
    const automobile_vin = data.autos
    console.log(automobile_vin)
        const sold_automobile = `http://localhost:8100${automobile_vin}`
        const soldConfig = {
            method:"PUT",
            body: JSON.stringify({ 'sold': true}),
            headers: {
                "Content-Type": "application/json",
            }
        }
    const customer = await response.json()
    console.log(customer)
    setAutomobile('')
    setCustomer('')
    setSalesPerson('')
    setSalePrice('')
    }
}
useEffect(() => {
    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/salespeople/');
            if (response.ok) {
                const { salespeople } = await response.json();
                setSalespeople(salespeople)

            }

            const saleAutoResponse = await fetch("http://localhost:8100/api/automobiles/");
            if (saleAutoResponse.ok) {
                const info = await saleAutoResponse.json();
                setAutomobiles(info.autos);
                

            }

            const customerResponse = await fetch('http://localhost:8090/api/customers/');
            if (customerResponse.ok) {
                const data = await customerResponse.json();
                setCustomers(data.customers);

            }




        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };

    fetchData();
}, []);



    return(
        <>
        <h1>Record a new sale</h1>
        <form onSubmit={handleSubmit}>


        <div className="dropDown">
    <select value={automobile}
    onChange={handleAutomobileChange}
    className='form-select'
    name = 'automobile'
    required type="text">
    <option value="">Choose an Automobile VIN...</option>
            {automobiles.map((vin) => (
              <option key={vin.id}
                value={vin.vin}>
                {vin.vin}
              </option>
            ))}
        </select>
    </div>

    <div className="dropDown">
    <select value={customer}
    onChange={handleCustomerChange}
    className = 'form-select'
    name= 'customer'
    required type="text">
    <option value="">Choose a Customer...</option>
            {customers.map((customer => (
              <option key={customer.id}
                value={customer.id}>
                {`${customer.first_name} ${customer.last_name}`}
              </option>
            )))}
        </select>
    </div>



    <div className="dropDown">
    <select value={sales_person}
    onChange={handleSalesPersonChange}
    className = 'form-select'
    name = 'sales_person'
    required type="text">
            <option value="">Choose a salesperson...</option>
            {salespeople.map((salesperson => (
              <option key={salesperson.id}
                value={salesperson.id}>
                {`${salesperson.first_name} ${salesperson.last_name}`}
              </option>
            )))}
        </select>
    </div>


    <div>
        <input onChange={handleSalePriceChange} value={sale_price} name="sale_price"
        placeholder="0" required type="text" id="sale_price" className='form-control'/>
        <label htmlFor="sale_price"></label>
    </div>

    <button className="btn btn-primary">Create</button>

        </form>

        </>
    );
}

export default CreateSale;
