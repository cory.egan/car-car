import React, {useState} from 'react';

function CreateCustomer() {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeId] = useState('')


    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value)
    }
    const handleEmployeeId = (e) => {
        const value = e.target.value;
        setEmployeeId(value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();

    const data = {
        first_name,
        last_name,
        employee_id,
    }


const url = 'http://localhost:8090/api/salespeople/';
const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
    }
}

const response = await fetch(url, fetchConfig)
if (response.ok) {
    const customer = await response.json()
    console.log(customer)
    setFirstName('')
    setLastName('')
    setEmployeeId('')
    }
}



return (
    <>
<h1>Create a Salesperson</h1>
<form onSubmit={handleSubmit}>
    <div>
        <input onChange={handleFirstNameChange} value={first_name} name="firstName" placeholder="First name..." required type="text" id="first_name" className='form-control'/>
        <label htmlFor="first_name"></label>
    </div>
    <div>
        <input onChange={handleLastNameChange} value={last_name} name="lastName" placeholder="Last name..." required type="text" id="last_name" className='form-control'/>
        <label htmlFor="last_name"></label>
    </div>
    <div>
        <input onChange={handleEmployeeId} value={employee_id} name="employeeID" placeholder="EmployeeID" required type="text" id="employee_id" className='form-control'/>
        <label htmlFor="employee_id"></label>
    </div>
    <button className="btn btn-primary">Create</button>
</form>
    </>

)




}
export default CreateCustomer;
