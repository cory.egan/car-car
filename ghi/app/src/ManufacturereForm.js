import React, { useState } from 'react';

function CreateManufacturerForm() {
  const [name, setName] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch('http://localhost:8100/api/manufacturers/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name }),
    });

    if (response.ok) {
      console.log("Manufacturer created successfully");
    } else {
      console.error("An error occurred while creating the manufacturer");
    }
  };

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <div className="card shadow">
            <div className="card-body">
              <h2 className="card-title">Create a Manufacturer</h2>
              <form onSubmit={handleSubmit} className="mt-4">
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="manufacturerName"
                    placeholder="Manufacturer name..."
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <label htmlFor="manufacturerName">Manufacturer Name</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateManufacturerForm;