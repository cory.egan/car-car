import React, { useEffect, useState } from 'react';

function Salespeople() {
    const [salespeople, setSalespeople] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:8090/api/salespeople/');
                if (response.ok) {
                    const { salespeople } = await response.json();
                    setSalespeople(salespeople);
                }
            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, []);

    return (
        <><h1>Salespeople</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(salesperson => {
                    return(

                        <tr key={salesperson.id}>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                    )
                    })}

            </tbody>
        </table></>
    );
}

export default Salespeople;
