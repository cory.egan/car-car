import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [inventory, setInventory] = useState([]);
  const [searchVin, setSearchVin] = useState('');
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    fetchAppointments();
    fetchInventory();
  }, []);

  useEffect(() => {

    const filtered = searchVin
      ? appointments.filter(appointment => appointment.vin.includes(searchVin))
      : appointments;
    setFilteredAppointments(filtered);
  }, [appointments, searchVin]);


  const fetchAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (!response.ok) {
        console.error("Failed to fetch appointments:", response.status);
        return;
      }
      const data = await response.json();
      setAppointments(data.appointments);
    } catch (error) {
      console.error("Error fetching appointments:", error);
    }
  };

  const fetchInventory = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (!response.ok) {
        console.error("Failed to fetch inventory:", response.status);
        return;
      }
      const data = await response.json();
      setInventory(data.automobiles);
    } catch (error) {
      console.error("Error fetching inventory:", error);
    }
  };

  const isVip = (vin) => {
    if (!inventory || inventory.length === 0) {
      return 'No';
    }
    return inventory.some(auto => auto.vin === vin && auto.sold) ? 'Yes' : 'No';
  };


  const handleSearch = (e) => {
    e.preventDefault();
    const filtered = appointments.filter(appointment => appointment.vin.includes(searchVin));
    setFilteredAppointments(filtered);
  };

  return (
    <div className='container'>
      <h1>Service History</h1>
      <form onSubmit={handleSearch}>
        <input
          type="text"
          placeholder="Search by VIN"
          value={searchVin}
          onChange={(e) => setSearchVin(e.target.value)}
        />
        <button type="submit">Search</button>
      </form>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date & Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map(appointment => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleString()}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{isVip(appointment.vin)}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;