import React, { useState, useEffect } from 'react';

function VehicleModelList() {
    const [vehicleModels, setVehicleModels] = useState([]);

    const fetchData = async () => {
        try {
            const response = await fetch("http://localhost:8100/api/models");
            if (response.ok) {
                const data = await response.json();
                setVehicleModels(data.models); 
            } else {
                console.error('HTTP Error:', response.statusText);
            }
        } catch (error) {
            console.error('Network Error:', error.message);
        }
    };

    useEffect(() => {
        fetchData();
    }, []); 

    return (
        <div className="container mt-4">
            <h1 className="text-center mb-4">Vehicle Models</h1>
            <div className="row">
                {vehicleModels.map((model) => (
                    <div key={model.id} className="col-md-4 mb-4">
                        <div className="card h-100">
                            <img src={model.picture_url} alt={model.name} className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">{model.name}</h5>
                                <p className="card-text">{model.manufacturer.name}</p>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default VehicleModelList;
