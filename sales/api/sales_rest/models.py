from django.db import models
from django.urls import reverse

# Value object to represent Inventory
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, blank=True, null=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return str(self.vin)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__ (self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_salespeople", kwargs={"id": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length = 15, unique=True)

    def __str__ (self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_customer", kwargs={"id": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    sales_person = models.ForeignKey(
        Salesperson,
        related_name="sales_person",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE,
    )
    sale_price = models.DecimalField(max_digits=25, decimal_places=2)

    def get_api_url(self):
        return reverse("api_sale", kwargs={"id": self.id})
