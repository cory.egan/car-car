from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    sold=models.BooleanField(default=False)
    def __str__(self):
        return self.vin
    
class Technician(models.Model):
    first_name=models.CharField(max_length=100)
    last_name=models.CharField(max_length=100)
    employee_id=models.CharField(max_length=100)

    def __str__(self):
        return f"{self.employee_id}: {self.first_name} {self.last_name}"

class Appointment(models.Model):
    vin=models.CharField(max_length=100)
    customer=models.CharField(max_length=100)
    date_time=models.DateTimeField(null=True, blank=True)
    reason=models.CharField(max_length=100)
    technician= models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
    status=models.CharField(max_length=100)

