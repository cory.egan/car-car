from django.urls import path
from .views import api_list_technician, api_show_technician, api_list_appointments, api_cancel_appointment, api_finish_appointment, api_appointment_detail, api_technician_detail

urlpatterns = [
    path('technicians/', api_list_technician, name='list_technicians'),
    path('technicians/<int:pk>/', api_show_technician, name='show_technician'),
    path('appointments/', api_list_appointments, name='list_appointments'),
    path('appointments/<int:pk>/cancel/', api_cancel_appointment, name='cancel_appointment'),
    path('appointments/<int:pk>/finish/', api_finish_appointment, name='finish_appointment'), 
    path('appointments/<int:pk>/', api_appointment_detail , name='delete_appointment'), 
    path('technicians/<int:pk>/', api_technician_detail , name='technician_detail'), 
]