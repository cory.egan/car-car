from django.shortcuts import render
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties= ["vin",
                 "sold"]

class TechnicianEncoder(ModelEncoder):
    model=Technician
    properties=[
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]

class AppointmentEncoder(ModelEncoder):
    model=Appointment
    properties = [
        "id",
        "customer",
        "vin",
        "reason",
        "date_time",
        "technician",
        "status",
    ]
    encoders = {
        "technician":TechnicianEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
                )
    else: 
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Invalid Employee number."},
                status=400
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "The technician you are looking for is not exist."},
                status=404
            )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse({"message": "Technician deleted"}, status=200)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "The technician you are trying to delete does not exist."},
                status=404
            )
    else: 
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message":"The technician you are trying to update does not exist."},
                status=404
            )
        
@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": list(appointments)},
            encoder=AppointmentEncoder,
            safe=False,
            content_type="application/json"
        )
    else:
        try:
            content = json.loads(request.body)
            technician_id = content.get("technician")
            technician = Technician.objects.get(employee_id=technician_id)

            status = content.get("status", "pending") 

            appointment = Appointment.objects.create(
                vin=content.get("vin"),
                customer=content.get("customer"),
                date_time=content.get("date_time"),
                reason=content.get("reason"),
                technician=technician,
                status=status,  
            )

            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"}, status=404)
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON format"}, status=400)
        except KeyError as e:
            return JsonResponse({"message": f"Missing field: {e}"}, status=400)




@require_http_methods(["DELETE"])
def api_technician_detail(request, pk):
    count, _ = Technician.objects.filter(id=pk).delete()
    if count == 0:
        return JsonResponse({"message": "Technician not found"}, status=404, content_type="application/json")
    return JsonResponse({"deleted": True}, content_type="application/json")



@require_http_methods(["DELETE"])
def api_appointment_detail(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.delete()
        return JsonResponse({"message": "Appointment deleted"}, status=200)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404, content_type="application/json")
    


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.status = 'canceled'  
        appointment.save()
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)



@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.status = 'finished'  
        appointment.save()
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)
